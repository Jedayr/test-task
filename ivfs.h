#ifndef IVFS_H
#define IVFS_H

#include <crtdefs.h>
#include <map>
#include <vector>
#include <mutex>

const size_t PhysicalSizeMax = 1000; // на самом деле это не максимальный размер файла, но если текущий размер
// больше этого, то новый виртуальный файл в этом физическом файле не создастся.
namespace TestTask
{
enum status {
    closed,
    openWriteOnly,
    openReadOnly
};
enum writeMode {
    replace,
    append
};

struct File {
    // основная проблема с многопоточностю - открытие в нескольких потоках одного физического
    // или виртуального файла.
    // возможные решения - использовать mutex или atomic
    // я не могу сказать, какое решение лучше, без долгих тестов.
    status isOpen = closed;
    size_t sizeActual;
    size_t sizeReserved;
    size_t sizeTechinal;
    //char* content;
    std::vector<char> content; // чтобы обойти большинство проблем с менеджментом памяти.
    std::string name;
    std::string techinal;

    size_t positionInPhysicalFile;
    bool isEverWritten = false;
    bool isLast = true;
    int numberOfPhysicalFile;

}  ; // Вы имеете право как угодно задать содержимое этой структуры

struct PhysicalFile {
    std::string physicalFilePath;
    std::map<std::string, File*> content;
    std::vector<std::string> names;
    void addFile(std::string name, int numberOfPhysicalFile);
    size_t currentSize=0;
};


struct IVFS
{    
    static std::mutex mutex;
    std::vector<PhysicalFile> files; // вместо продвинутой структуры
    File *Open( const std::string name ); // Открыть файл в readonly режиме. Если нет такого файла или же он открыт во writeonly режиме - вернуть nullptr
    File *Create( const std::string  name ); // Открыть или создать файл в writeonly режиме. Если нужно, то создать все нужные поддиректории, упомянутые в пути. Вернуть nullptr, если этот файл уже открыт в readonly режиме.
    size_t Read( File *f, char *buff, size_t len ); // Прочитать данные из файла. Возвращаемое значение - сколько реально байт удалось прочитать
    size_t Write( File *f, char *buff, size_t len, writeMode mode = replace ); // Записать данные в файл. Возвращаемое значение - сколько реально байт удалось записать

    size_t WriteAppend( File *f, char *buff, size_t len ); //

    void Close( File *f ); // Закрыть файл


    void PrintFilesStructure(std::string path); // для теста
    static IVFS CreateStructureFromFile(std::string path); // для теста
};

}


#endif // IVFS_H
