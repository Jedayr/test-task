#include <iostream>
#include "ivfs.h"
#include <cassert>
#include <thread>
using namespace std;


std::string content = "123456789101112131415";

bool checkRead = false;
bool checkThreads = false;
void testRead() {
// проверяется, что при 1) записывани в физический файл содержимого 2) сохранения структуры с этим содержимым
// 3) импорта структуры в другую систему 4) считывания содержимого файлов
// считывается ровно то что было записано
    TestTask::IVFS temp;
    TestTask::File * tempFile, *tempFile1 ;
    for(int i = 0; i < 6; ++i) {
        std::string name = "file"+std::to_string(i);
        tempFile = temp.Create(name);
        temp.Write(tempFile,content.data(),i+4);
        temp.Close(tempFile);
    }
    temp.PrintFilesStructure("testread");
    TestTask::IVFS temp1 = TestTask::IVFS::CreateStructureFromFile("testread");
    for(int i = 0; i <6; ++i) {
        std::string name = "file"+std::to_string(i);
        tempFile = temp.Open(name);
        tempFile1 = temp1.Open(name);
        temp1.Read(tempFile1,content.data(),tempFile->sizeActual);
        temp.Close(tempFile);
        temp1.Close(tempFile1);
        assert(tempFile->sizeActual == tempFile1->sizeActual);
        std::cout << "file " << i << " size is same for read and write" <<std::endl;;
    }
}

TestTask::IVFS temp;
TestTask::File * tempFile ;
int counter = 12;

void createFile() {
    std::string name = "file"+std::to_string(counter);
    ++counter;
    tempFile = temp.Create(name);
    int result = temp.Write(tempFile,content.data(),counter);
    temp.Close(tempFile);
    std::cout << "file " << name << (result > 0 ?" written" :" not written") <<std::endl;;
}
int main()
{
    // код, с котоырм проверялась возможность записать содержимое нескольких файлов
    // и замена содержимого одного из них без перезаписывания всего.
/*    for(int i = 0; i < 12; ++i) {
        std::string name = "file"+std::to_string(i);
        tempFile = temp.Create(name);
        temp.Write(tempFile,content.data(),i+4);
        temp.Close(tempFile);
    }
    std::cout << "created 12 files" <<std::endl;
    tempFile = temp.Create("file6");
    std::string tempString = "hello world! there";
    temp.Write(tempFile,tempString.data(),tempString.size());*/
    TestTask::File *f1 = temp.Create("f1.txt");
    TestTask::File *f2 = temp.Create("f2.txt");
    temp.Write(f1, std::string("03").data(), 2 );
    temp.Write(f2, std::string("a").data(), 1);
    temp.Write(f1, std::string("1").data(), 1 );
    temp.Write(f1, std::string("2").data(), 1, TestTask::writeMode::append  );
    temp.Write(f2, std::string("b").data(), 1, TestTask::writeMode::append );
    temp.Close(f1);
    temp.Close(f2);
    std::cout << "changed text in one file" << std::endl;;
    // от этого кода в тестовый файл добавились еще 3 файла последовательно - 12, 13, 14
    // в случае полной неприспособленности к работе с потоками
    // записалось бы несколько файлов 12.
    // скорее всего, решение с потоками неидеально.
    if(checkThreads){
        std::thread file1(createFile);
        std::thread file2(createFile);
        std::thread file3(createFile);

        file1.join();
        file2.join();
        file3.join();
    }

    if(checkRead) {
        testRead();
    }

    return 0;
}
