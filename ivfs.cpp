#include "ivfs.h"
#include <ios>
#include <fstream>
#include <sstream>

//static define
std::mutex TestTask::IVFS::mutex;
TestTask::File *TestTask::IVFS::Open(const std::string  name)
{
    // чтобы не открывали два потока одновременно
    mutex.lock();
    //поиск файла
    for(int i = 0; i < files.size(); ++i) {
        auto f = files[i].content.find(name);
        if(f != files[i].content.end()) {
        //файл существует и можно открыть - меняется статус
            f->second->isOpen = openReadOnly;
            mutex.unlock();
            return f->second;
        }
    }
    mutex.unlock();
    // файла нет или занят - вернуть nullptr
    return nullptr;
}

TestTask::File *TestTask::IVFS::Create(const std::string name)
{
    //тут поиск файла на открытие
    for(int i = 0; i < files.size(); ++i) {
        auto f = files[i].content.find(name);
        if(f != files[i].content.end()) {
        //файл существует и можно открыть
            if(f->second->isOpen == openReadOnly) return nullptr;
            f->second->isOpen = openWriteOnly;
            return f->second;
        }
    }
    mutex.lock();

    //файла не существует - надо добавить
    int treeNumber = 0;
    for(treeNumber = 0; treeNumber < files.size();++treeNumber) {
        if(files[treeNumber].currentSize < PhysicalSizeMax) {
            break;
        }
    }
    // режим для добавления в конец файла
    std::ios_base::openmode mode = std::ios_base::binary | std::ios_base::app | std::ios_base::out;
    // если все физические файлы заняты - надо сделать новое.
    if (treeNumber == files.size()) {
        files.push_back(PhysicalFile());
        files.back().physicalFilePath = "temp"+std::to_string(treeNumber);
        // перезаписывать, чтобы не держать хвост из прошлых запусков программы - для не тестового
        // надо будет изменить формат названий файлов.
        mode = std::ios_base::binary | std::ios_base::trunc | std::ios_base::out;
    }


    // очищается физический файл, если пусто.
    std::ofstream ofs(files[treeNumber].physicalFilePath,mode);
    if(ofs.is_open()) {
        ofs.close();
    }
    //ставиться статус файла и возвращается файл
    files[treeNumber].addFile(name,treeNumber);
    files[treeNumber].content[name]->isOpen = openWriteOnly;
    files[treeNumber].content[name]->isEverWritten = false;
    files[treeNumber].content[name]->name = name;
    files[treeNumber].content[name]->techinal = "name:"+name+";size:";
    files[treeNumber].content[name]->sizeTechinal = files[treeNumber].content[name]->techinal.size();
    mutex.unlock();

    return files[treeNumber].content[name];
}

size_t TestTask::IVFS::Read(File *f, char *buff, size_t len)
{
    size_t size = 0;
    if (f->isOpen != openReadOnly) return size;
    std::ifstream is(files[f->numberOfPhysicalFile].physicalFilePath) ;
    if(is.is_open()) {
        long pos = f->positionInPhysicalFile;
        // причина: обрабатываю случай, что файл изменился в плане актуального размера. иначе можно было бы
        // сделать pos = f->positionInPhysicalFile + f->sizeTechinal -1
        is.seekg(pos,std::ios_base::beg);
        size = std::min(len + f->sizeTechinal +1,f->sizeReserved);
        char * temp = new char[size];
        is.read(temp,size);
        int counter = 0;;
        bool isChanged = false;
        for(counter = 0; counter<f->sizeTechinal -1;++counter) {
            if(temp[counter] == f->techinal[counter] ) continue; // значит что тех.информация совпадает -
            // можно использоватть sizeactual как верный.
            // различия в буквах - читается не то и не там.
            if (temp[counter] >='0' && temp[counter]<='9') {
                isChanged = true; break;
            } else {
                // различаются буквы - считывается не тот файл.
                is.close();
                return 0;
            }
        }
        if(isChanged) {
            std::string tempString("name"+f->name+";size:");
            counter = tempString.length();
            tempString = "";
            while(temp[counter]!=';') {
                tempString+=temp[counter];
                ++counter;
            }
            f->sizeActual = std::stoi(tempString);
            f->techinal ="name"+f->name+";size:" + tempString + ";";
            f->sizeTechinal = f->techinal .length();
            f->content.resize(f->sizeActual);
        }
        // считывание нового текста в файл, сюда же можно добавить копирование в буфер
        // запись этого в виртуальный файл
        size = 0;
        while(temp[counter]!='\0') {
            f->content[size] = temp[counter];
            ++size;
            ++counter;
        }
        size = counter;
        delete [] temp;
        is.close();
    }
    return size;
}

size_t TestTask::IVFS::Write(File *f, char *buff, size_t len, writeMode mode)
{  
    size_t result = 0;
    // файл не открыт на запись - записано 0.
    if (f->isOpen != openWriteOnly) return result;  
    // файл открывается, чтобы можно было заменить часть содержимого.
    std::fstream fs(files[f->numberOfPhysicalFile].physicalFilePath, std::ios_base::binary | std::ios_base::out | std::ios_base::in);
    if(fs.is_open()) {
        mutex.lock();
        //начало текущего файла в физическом файле.
        if(!f->isEverWritten) {
            f->positionInPhysicalFile = files[f->numberOfPhysicalFile].currentSize;
            for(int i = 0; i < files[f->numberOfPhysicalFile].names.size();++i) {
                files[f->numberOfPhysicalFile].content[files[f->numberOfPhysicalFile].names[i]]->isLast = false;
            }
            f->isLast = true;
            f->isEverWritten = true;
        }
        long pos = f->positionInPhysicalFile;;
        // запись содержимого буфера в файл и физический и виртуальный.
        fs.seekp(pos,std::ios_base::beg);
        if(f->isLast) {
            result = mode == replace? len : len + f->content.size();
            f->techinal = "name:" + f->name + ";size:" + std::to_string(result)+";";
            f->sizeTechinal = f->techinal.size() + 1;
            f->sizeReserved = f->sizeTechinal + result + 1;
            files[f->numberOfPhysicalFile].currentSize +=f->sizeReserved;
        } else {
            if(mode == replace) {
                result = std::min(len,f->sizeReserved - f->sizeTechinal);
            } else {
                result = std::min(len+f->sizeActual,f->sizeReserved - f->sizeTechinal);
            }
            f->techinal = "name:" + f->name + ";size:" + std::to_string(result)+";";
            f->sizeTechinal = f->techinal.size() + 1;
        }
        fs.write(f->techinal.data(),f->sizeTechinal-1);
        if(mode == append) {
            fs.write(f->content.data(),f->sizeActual);
            fs.write(buff,result - f->sizeActual);
        } else {
            fs.write(buff,result);
        }
        fs.write("\0",1);
        f->content.resize(result);
        for(int i = mode == replace? 0 : f->sizeActual; i < result; ++i) {
            f->content[i] = buff[i];
        }
        if(mode == append) {
            f->sizeActual = result;
        } else {
            f->sizeActual = result;
        }
        fs.close();
        mutex.unlock();

    }
    return result;
}

size_t TestTask::IVFS::WriteAppend(File *f, char *buff, size_t len)
{
    return Write(f,buff,len,append);
}

void TestTask::IVFS::Close(File *f)
{
    // закрытие файла.
    f->isOpen = closed;
}

// методы для тестирования чтения из файла.
void TestTask::IVFS::PrintFilesStructure(std::string path)
{
    std::ofstream ofs(path);
    if(ofs.is_open()) {
        ofs << files.size() << std::endl;
        for(int i = 0; i < files.size();++i) {
            ofs << files[i].physicalFilePath << std::endl;
            ofs << files[i].names.size() << std::endl;
            for(int j = 0; j <files[i].names.size(); ++j) {
                ofs << files[i].names[j] << std::endl;
                ofs << files[i].content[files[i].names[j]]->positionInPhysicalFile
                   << " "<< files[i].content[files[i].names[j]]->sizeTechinal
                   << " " << files[i].content[files[i].names[j]]->sizeActual
                   << std::endl;
            }
        }
        ofs.close();
    }

}

TestTask::IVFS TestTask::IVFS::CreateStructureFromFile(std::string path)
{
    IVFS result;
    std::ifstream ifs(path);
    if(ifs.is_open()) {
        std::string temp;
        std::getline(ifs,temp);
        int amountOfFiles = std::stoi(temp);
        for(int i = 0; i < amountOfFiles; ++i) {
            result.files.push_back(PhysicalFile());
            int count;
            std::getline(ifs,temp);
            result.files[i].physicalFilePath = temp;
            std::getline(ifs,temp);
            count = std::stoi(temp);
            for(int j = 0; j < count; ++j) {
                std::getline(ifs,temp);
                result.files[i].addFile(temp,i);
                std::getline(ifs,temp);
                std::istringstream iss(temp);
                iss >> result.files[i].content[result.files[i].names[j]]->positionInPhysicalFile
                        >> result.files[i].content[result.files[i].names[j]]->sizeTechinal
                        >> result.files[i].content[result.files[i].names[j]]->sizeActual;

            }
        }
        ifs.close();
    }
            return result;
}



void TestTask::PhysicalFile::addFile(std::string name, int numberOfPhysicalFile)
{
    names.push_back(name);
    content.emplace(name,new File());
    content[name]->numberOfPhysicalFile = numberOfPhysicalFile;
    content[name]->sizeActual = 0;
}
